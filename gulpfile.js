/**
 * Created by karbunkul on 03/03/15.
 */

var gulp = require('gulp');
var sass = require('gulp-sass');
var replace = require('gulp-replace');
var rm = require('gulp-rimraf');

gulp.task('default', ['bower']);

gulp.task('clean', function() {
    gulp.src([
        'assets'
    ]).pipe(rm({ force: true }));
});

/**
 * Get last version from bower
 */
gulp.task('bower', function() {
    gulp.src([
        'bower_components/semantic-ui/dist/components/**/*.min.css',
        'bower_components/semantic-ui/dist/semantic.min.css'
    ]).pipe(gulp.dest('assets/css'));

    gulp.src('bower_components/semantic-ui/dist/themes/**/*')
        .pipe(gulp.dest('assets/themes'));
});