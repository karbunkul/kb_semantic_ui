# Описание
Библиотека Semantic UI для Drupal 7 
# Установка
`sudo npm install && sudo bower install && gulp bower && gulp`

# Использование
- `drupal_add_library(‘kb_semantic_ui’, ‘semantic’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘accordion’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘ad’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘breadcrumb’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘button’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘card’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘checkbox’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘divider’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘comment’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘dimmer’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘divider’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘dropdown’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘feed’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘flag’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘form’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘grid’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘header’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘icon’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘image’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘input’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘item’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘label’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘list’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘loader’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘menu’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘message’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘modal’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘nag’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘popup’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘progress’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘rail’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘rating’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘reset’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘reveal’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘search’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘segment’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘shape’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘sidebar’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘site’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘statistic’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘step’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘sticky’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘tab’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘table’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘transition’);`
- `drupal_add_library(‘kb_semantic_ui’, ‘video’);`

# Автор 
© 2015 Александр Походюн ([Karbunkul](mailto://karbunkul@yourtask.ru))
# Лицензия
MIT